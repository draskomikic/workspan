# WorkSpan Homework Question v2

A python program that clones an entity and all its related entities in 
the manner of directional graph.

The program takes in:
 - JSON file as first input representing the entities and the links
 - id of the entity that needs to be cloned as second input.

The program can be set up and ran as python script in virtual environment
or as python script in Docker container

## Using virtual environment

### Requirements
- Python 3.7
- pip (https://pip.pypa.io/en/stable/installing/)

Use pip to install `virtaulenvwrapper`. Python environment will isolate 
homework dependencies from OS python libraries. 
```
pip install virtualenvwrapper==4.8.4
```

Create new python virtual environment called `workspan-homework` and install
requirements using python version 3.
```
mkvirtualenv --python=$(which python3) workspan-homework
```

In case `mkvirtualenv` command does not automatically set shell user in 
the virtual environment context run this command:
```
workon workspan-homework
``` 

Get help to run homework script:
```
python src/homework.py -h
```

Run homework script in format:
```
python src/homework.py [path_to_json_file] [entity_id]
```

Example JSON file is provided in the source code folder with the examples
from the homework assignment.
```
python src/homework.py src/example.json 5
```

To run unit tests execute following command from project root folder:
```
python -m unittest
```

## Using Docker container

### Requirements
- Docker app


Create new Docker network called `workspan-homework-network`
```
docker network create workspan-homework-network
```

Build the container and install all requirements
```
docker-compose build
```

Run the container with the example JSON file is provided in the source 
code folder with the examples from the homework assignment.
```
docker-compose run workspan-homework
```

Run the custom command in the following format. Make sure that json file 
is placed in the `src` folder in the project since docker-compose.yml makes
volume using that folder.
```
docker-compose run workspan-homework python src/homework.py [path_to_json_file] [entity_id]
```

To run unit tests execute following command:
```
docker-compose run workspan-homework python -m unittest
```
