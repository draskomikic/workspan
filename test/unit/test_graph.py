import json
import unittest

from entity import Entity
from exception import CyclicEntitiesError
from graph import Graph


class GraphTest(unittest.TestCase):

    def setUp(self):
        self.graph = Graph()

    def _generate_entity(self):
        new_id = max(self.graph.entity_list.keys()) + 1 if len(self.graph.entity_list.keys()) else 1
        return Entity(new_id, f"Test entity {new_id}", f"Some test description for entity {new_id}")

    def test_add_entity_to_graph(self):
        entity = self._generate_entity()
        self.graph.add_entity(entity)
        self.assertEqual(self.graph.get_entity(1).id, entity.id)

    def test_add_link_to_graph(self):
        entity1 = self._generate_entity()
        self.graph.add_entity(entity1)
        entity2 = self._generate_entity()
        self.graph.add_entity(entity2)
        entity3 = self._generate_entity()
        self.graph.add_entity(entity3)

        self.graph.add_link(entity1, entity2)
        self.graph.add_link(entity2, entity3)

    def test_get_entity_from_graph(self):
        entity1 = self._generate_entity()
        self.graph.add_entity(entity1)
        entity2 = self._generate_entity()
        self.graph.add_entity(entity2)
        entity3 = self._generate_entity()
        self.graph.add_entity(entity3)

        self.graph.add_link(entity1, entity2)
        self.graph.add_link(entity2, entity3)
        example_entity = self.graph.get_entity(2)

        self.assertEqual(example_entity.id, entity2.id)
        self.assertIn(entity3, example_entity.links)
        self.assertIn(entity1, example_entity.predecessors)

    def test_clone_entity(self):
        entity1 = self._generate_entity()
        self.graph.add_entity(entity1)
        entity2 = self._generate_entity()
        self.graph.add_entity(entity2)
        entity3 = self._generate_entity()
        self.graph.add_entity(entity3)
        entity4 = self._generate_entity()
        self.graph.add_entity(entity4)

        self.graph.add_link(entity1, entity2)
        self.graph.add_link(entity1, entity3)
        self.graph.add_link(entity2, entity3)
        self.graph.add_link(entity3, entity4)

        self.graph.clone(entity2, entity2.predecessors)

        entity_clone_1 = self.graph.get_entity(5)
        entity_clone_2 = self.graph.get_entity(6)
        entity_clone_3 = self.graph.get_entity(7)

        self.assertIsNotNone(entity_clone_1)
        self.assertIsNotNone(entity_clone_2)
        self.assertIsNotNone(entity_clone_3)

        self.assertIn(entity1, entity_clone_1.predecessors)
        self.assertIn(entity_clone_2, entity_clone_1.links)
        self.assertIn(entity_clone_2, entity_clone_3.predecessors)
        self.assertIn(entity_clone_3, entity_clone_2.links)

    def test_clone_entity_stops_when_loop_is_detected(self):
        entity1 = self._generate_entity()
        self.graph.add_entity(entity1)
        entity2 = self._generate_entity()
        self.graph.add_entity(entity2)
        entity3 = self._generate_entity()
        self.graph.add_entity(entity3)
        entity4 = self._generate_entity()
        self.graph.add_entity(entity4)

        self.graph.add_link(entity1, entity2)
        self.graph.add_link(entity1, entity3)
        self.graph.add_link(entity2, entity3)
        self.graph.add_link(entity3, entity4)
        self.graph.add_link(entity4, entity2)

        self.assertRaises(CyclicEntitiesError, self.graph.clone, entity2, entity2.predecessors)

    def test_make_output_from_graph(self):
        entity1 = self._generate_entity()
        self.graph.add_entity(entity1)
        entity2 = self._generate_entity()
        self.graph.add_entity(entity2)
        entity3 = self._generate_entity()
        self.graph.add_entity(entity3)
        entity4 = self._generate_entity()
        self.graph.add_entity(entity4)

        self.graph.add_link(entity1, entity2)
        self.graph.add_link(entity2, entity3)
        self.graph.add_link(entity3, entity4)

        self.graph.clone(entity2, entity2.predecessors)
        output = {
            "entities": [],
            "links": [],
        }
        output = self.graph.make_output(entity1, output)
        output_dict = json.loads(output)

        entity_ids_list = [entity['entity_id'] for entity in output_dict['entities']]
        self.assertIn(1, entity_ids_list)
        self.assertIn(2, entity_ids_list)
        self.assertIn(3, entity_ids_list)
        self.assertIn(4, entity_ids_list)
        self.assertIn(5, entity_ids_list)
        self.assertIn(6, entity_ids_list)
        self.assertIn(7, entity_ids_list)

        from_links_list = [link['from'] for link in output_dict['links']]
        self.assertIn(1, from_links_list)
        self.assertIn(2, from_links_list)
        self.assertIn(3, from_links_list)
        self.assertIn(5, from_links_list)
        self.assertIn(6, from_links_list)

        to_links_list = [link['to'] for link in output_dict['links']]
        self.assertIn(2, to_links_list)
        self.assertIn(3, to_links_list)
        self.assertIn(4, to_links_list)
        self.assertIn(5, to_links_list)
        self.assertIn(6, to_links_list)
        self.assertIn(7, to_links_list)
