import unittest

from src.entity import Entity


class EntityTest(unittest.TestCase):

    def setUp(self):
        self.entity = Entity(entity_id=1, name='Test Entity', description='Some test description')

    def test_add_link(self):
        link_entity_data = [2, "Link Entity 2", "Some test description for link entity 2"]
        link_entity = Entity(*link_entity_data)
        self.entity.add_link(link_entity)
        self.assertEqual(1, len(self.entity.links))

        self.assertIn(link_entity, self.entity.links)

    def test_add_predecessor(self):
        predecessor_entity_data = [3, "Predecessor Entity 3", "Some test description for predecessor entity 3"]
        predecessor_entity = Entity(*predecessor_entity_data)
        self.entity.add_predecessor(predecessor_entity)
        self.assertEqual(1, len(self.entity.predecessors))

        self.assertIn(predecessor_entity, self.entity.predecessors)

    def test_entity_print_representation(self):
        expected_representation = f"{self.entity.id} links to: {[x.id for x in self.entity.links]} " \
            f"and has predecessors: {[x.id for x in self.entity.predecessors]}"
        self.assertEqual(expected_representation, str(self.entity))
