import unittest
from unittest.mock import patch, mock_open

from exception import JSONFileNotFoundError, JSONParsingError, EntitiesNotFound, LinksNotFound
from homework import load_json_file, load_json, load_from_dict


class HomeworkTest(unittest.TestCase):

    def test_load_json_file_raises_exception_when_bad_filepath(self):
        self.assertRaises(JSONFileNotFoundError, load_json_file, "bad_path")

    @patch('os.path.exists')
    def test_load_json_file_raises_exception_when_bad_json_file_format(self, os_path_exists_mock):
        os_path_exists_mock.return_value = True

        m = mock_open()
        with patch('homework.open', m):
            self.assertRaises(JSONParsingError, load_json_file, "some_path")

    @patch('homework.load_json_file')
    def test_load_json_raises_exception_when_entities_are_missing(self, homework_load_json_file_mock):
        homework_load_json_file_mock.return_value = {}
        self.assertRaises(EntitiesNotFound, load_json, "some_json_path")

    @patch('homework.load_json_file')
    def test_load_json_raises_exception_when_entities_list_is_empty(self, homework_load_json_file_mock):
        homework_load_json_file_mock.return_value = {"entities": []}
        self.assertRaises(EntitiesNotFound, load_json, "some_json_path")

    @patch('homework.load_json_file')
    def test_load_json_raises_exception_when_links_are_missing(self, homework_load_json_file_mock):
        homework_load_json_file_mock.return_value = {
            "entities": [
                {
                    'id': 1
                }
            ]
        }
        self.assertRaises(LinksNotFound, load_json, "some_json_path")

    @patch('homework.load_json_file')
    def test_load_json_raises_exception_when_links_list_is_empty(self, homework_load_json_file_mock):
        homework_load_json_file_mock.return_value = {
            "entities": [
                {
                    'id': 1
                }
            ],
            "links": []
        }
        self.assertRaises(LinksNotFound, load_json, "some_json_path")

    def test_load_from_dict(self):
        json_dict = {
            "entities": [{
                "entity_id": 3,
                "name": "EntityA"
            }, {
                "entity_id": 5,
                "name": "EntityB"
            }],
            "links": [{
                "from": 3,
                "to": 5
            }]
        }
        graph = load_from_dict(json_dict)

        self.assertIsNotNone(graph.get_entity(3))
        self.assertIsNotNone(graph.get_entity(5))
        self.assertIn(graph.get_entity(5), graph.get_entity(3).links)
