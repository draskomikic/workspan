from __future__ import annotations


class Entity:
    def __init__(self, entity_id: int, name: str, description: str = None) -> None:
        """
        :param entity_id: Entity id
        :type entity_id: int
        :param name: Entity name
        :type name: str
        :param description: Entity description
        :type description: str
        """
        self.id = entity_id
        self.name = name
        self.description = description
        self.links = set()
        self.predecessors = set()

    def add_link(self, to_entity: Entity) -> None:
        """
        Add link to related entity.
        
        :param to_entity: Entity instance to link to
        :type to_entity: Entity
        """
        self.links.add(to_entity)

    def add_predecessor(self, from_entity: Entity) -> None:
        """
        Add entity that links this entity.

        :param from_entity: Entity that links to this entity.
        :type from_entity: Entity
        """
        self.predecessors.add(from_entity)

    def __str__(self) -> str:
        return f"{self.id} links to: {[x.id for x in self.links]} " \
            f"and has predecessors: {[x.id for x in self.predecessors]}"
