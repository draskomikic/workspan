import argparse
import json
import os
import sys
from json import JSONDecodeError
from typing import Dict

from entity import Entity
from graph import Graph
from exception import JSONFileNotFoundError, EntityNotFound, EntitiesNotFound, LinksNotFound, JSONParsingError, \
    CyclicEntitiesError


def load_json_file(file_path: str) -> Dict:
    """
    Load JSON as dict from json file.

    :param file_path: Path to the JSON file
    :type file_path: str
    :return: JSON as dictionary
    :rtype: dict
    """
    if not os.path.exists(file_path):
        raise JSONFileNotFoundError(f"JSON file not found in path: {file_path}")

    try:
        with open(file_path, 'r') as file_handler:
            return json.load(file_handler)
    except JSONDecodeError:
        raise JSONParsingError("Invalid JSON file provided")


def load_json(json_file_path: str) -> Dict:
    """
    Load and check if loaded JSON from file has entities and links.

    :param json_file_path: JSON file path to parse
    :type json_file_path: str
    :return: JSON as dictionary
    :rtype: dict
    """
    loaded_json = load_json_file(json_file_path)

    if not loaded_json.get('entities'):
        raise EntitiesNotFound("Entities list doesn't exist")
    if loaded_json.get('entities') == []:
        raise EntitiesNotFound("Entities list is empty")
    if not loaded_json.get('links'):
        raise LinksNotFound("Links list doesn't exist")
    if loaded_json.get('links') == []:
        raise LinksNotFound("Links list is empty")

    return loaded_json


def load_from_dict(json_input: Dict) -> Graph:
    """
    Construct a Graph from parsed JSON as dictionary.

    :param json_input: Dictionary of entities and links
    :type: dict
    :return: New directional graph
    :rtype: Graph
    """
    g = Graph()
    for item in json_input['entities']:
        entity = Entity(
            entity_id=item.get('entity_id'),
            name=item.get('name'),
            description=item.get('description')
        )
        g.add_entity(entity)

    for item in json_input['links']:
        from_entity = g.get_entity(item['from'])
        to_entity = g.get_entity(item['to'])
        g.add_link(from_entity, to_entity)

    return g


def do_homework(json_file_path: str, entity_id: int) -> Dict:
    """
    Main program function.

    :param json_file_path: File path to JSON file to parse from.
    :type json_file_path: str
    :param entity_id: Id of Entity to clone from
    :type entity_id: int
    """
    json_dict = load_json(json_file_path)
    graph = load_from_dict(json_dict)

    given_entity = graph.get_entity(entity_id)
    if not given_entity:
        raise EntityNotFound(f"Given entity id {entity_id} does not exist in entity list in JSON file")

    graph.clone(given_entity, given_entity.predecessors)

    start_entity = graph.get_entity(min(graph.entity_list.keys()))
    output = {
        "entities": [],
        "links": [],
    }
    return graph.make_output(start_entity, output)


if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(description='A program to clone entity and all its related entities.')
        parser.add_argument("json_file", metavar='file', type=str, help='Path to a JSON file with entities and links')
        parser.add_argument("entity_id", metavar='id', type=int, help='Entity identifier (integer)')
        args = parser.parse_args()
        output_json = do_homework(args.json_file, args.entity_id)
        print(output_json)
    except CyclicEntitiesError as ce_error:
        print(ce_error)
        sys.exit(1)
    except Exception as exception:
        print(exception)
        sys.exit(1)
