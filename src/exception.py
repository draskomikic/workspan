class BaseError(Exception):
    default_message_prefix = "ERROR"

    def __init__(self, message: str) -> None:
        self.message = message

    def __str__(self) -> str:
        return f"{self.default_message_prefix}: {self.message}"


class JSONParsingError(BaseError):
    pass


class EntityNotFound(BaseError):
    pass


class EntitiesNotFound(BaseError):
    pass


class LinksNotFound(BaseError):
    pass


class JSONFileNotFoundError(BaseError):
    pass


class CyclicEntitiesError(BaseException):
    pass
