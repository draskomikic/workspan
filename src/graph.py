import json
from typing import Optional, List, Set, Dict, Iterator

from entity import Entity
from exception import CyclicEntitiesError, EntityNotFound


class Graph:
    def __init__(self) -> None:
        self.entity_list = {}
        self.traversal_list = {}

    def add_entity(self, entity: Entity) -> None:
        """
        :param entity: Entity to add to Graph
        :type entity: Entity
        :return:
        """
        if isinstance(entity, Entity):
            self.entity_list[entity.id] = entity
            self.traversal_list[entity.id] = "not_visited"

    def get_entity(self, entity_id: int) -> Optional[Entity]:
        """
        :param entity_id: Id of Entity
        :type entity_id: Entity
        :return: Entity or None
        """
        if entity_id in self.entity_list.keys():
            return self.entity_list[entity_id]

        return None

    def add_link(self, from_entity: Entity, to_entity: Entity) -> None:
        """
        Add Entity to the list of related entities.

        :param from_entity: Entity where link will be added
        :param to_entity: Entity to be added as link
        """
        try:
            if isinstance(from_entity, Entity) and isinstance(to_entity, Entity):
                self.entity_list[from_entity.id].add_link(to_entity)
                self.entity_list[to_entity.id].add_predecessor(from_entity)
        except KeyError as key_error:
            raise EntityNotFound(f'Graph does not contains entity with id: {key_error}')

    def clone(self, entity: Entity, predecessors_list: List, visited: Set = None) -> None:
        """
        Clone given entity and all its related entities.
        Exits in case it detects loop in entities links.

        :param entity: Entity to clone
        :type entity: Entity
        :param predecessors_list: List of predecessor Entities
        :type predecessors_list: list
        :param visited: Set of visited entities
        :type visited: set
        """
        try:
            if visited is None:
                visited = set()
            new_id = max(self.entity_list.keys()) + 1
            new_entity = Entity(new_id, entity.name, entity.description)
            for predecessor in predecessors_list:
                new_entity.add_predecessor(predecessor)
                predecessor.add_link(new_entity)

            self.entity_list[new_entity.id] = new_entity

            visited.add(entity.id)
            if self.traversal_list.get(entity.id):
                self.traversal_list[entity.id] = "visiting"

            for l in entity.links:
                if self.traversal_list.get(l.id) == "visiting":
                    raise CyclicEntitiesError("Loop in entities detected. Aborting.")
                if l.id not in visited and l.id in self.traversal_list:
                    self.clone(l, [new_entity], visited)

            if self.traversal_list.get(entity.id):
                self.traversal_list[entity.id] = "visited"
        except CyclicEntitiesError:
            raise

    def make_output(self, start_entity: Entity, output: Dict) -> Dict:
        """
        Outputs JSON structure of Graph with list of entities and links

        :param start_entity: Entity to start DFS from
        :type start_entity: Entity
        :param output: Dictionary with empty entities and links lists
        :type output: dict
        :return: JSON output
        :rtype: str
        """
        visited = set()

        stack = [start_entity]

        while stack:
            s = stack[-1]
            stack.pop()

            if s.id not in visited:
                visited.add(s.id)

            for node in self.entity_list[s.id].links:
                output["links"].append({
                    "from": s.id,
                    "to": node.id
                })
                if node.id not in visited:
                    stack.append(node)

            entity_output = {
                "entity_id": s.id,
                "name": s.name
            }

            if s.description:
                entity_output["description"] = s.description

            output["entities"].append(entity_output)

        return json.dumps(output)

    def __iter__(self) -> Iterator:
        return iter(self.entity_list.values())

    def __contains__(self, item: int) -> bool:
        return item in self.entity_list.keys()
